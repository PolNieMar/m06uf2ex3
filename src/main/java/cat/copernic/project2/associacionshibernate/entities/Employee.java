/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.project2.associacionshibernate.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Pol Nieto Mart�nez
 */

@Entity
@Table(name="employee")
public class Employee implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_empleat")
    private Long id;

    @Column(name = "nom_empleat", nullable = false)
    private String nombre;

    @Column(name = "alta_empresa", nullable = false)
    private Date fechaAltaEmpresa;

    @Column(name = "neixement_empleat", nullable = false)
    private Date fechaNacimiento;

    public Employee(Long id, String nombre, Date fechaAltaEmpresa, Date fechaNacimiento) {
        this.id = id;
        this.nombre = nombre;
        this.fechaAltaEmpresa = fechaAltaEmpresa;
        this.fechaNacimiento = fechaNacimiento;
    }

    public Employee() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaAltaEmpresa() {
        return fechaAltaEmpresa;
    }

    public void setFechaAltaEmpresa(Date fechaAltaEmpresa) {
        this.fechaAltaEmpresa = fechaAltaEmpresa;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", nombre=" + nombre + ", fechaAltaEmpresa=" + fechaAltaEmpresa + ", fechaNacimiento=" + fechaNacimiento + '}';
    }

}