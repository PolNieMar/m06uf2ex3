/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.project2.associacionshibernate;

import cat.copernic.project2.associacionshibernate.entities.Employee;
import com.github.javafaker.Faker;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
    // Declaraci�n de variables est�ticas
    private static SessionFactory factory;
    private static final Logger logger = LogManager.getLogger(App.class);
    private static Session session;

    public static void main(String[] args) {
        try {
            // Configuraci�n de Hibernate
            factory = new Configuration().configure("hibernateConfig/hibernate.cfg.xml").addAnnotatedClass(Employee.class).buildSessionFactory();
            session = factory.openSession();

            // Generaci�n de datos falsos con Java Faker
            Faker faker = new Faker();

            // Comienza la transacci�n para la inserci�n de registros
            session.beginTransaction();
            for (int i = 0; i < 1000; i++) {
                // Creaci�n de un empleado ficticio
                Employee employee = new Employee();
                employee.setNombre(faker.name().fullName());
                employee.setFechaAltaEmpresa(faker.date().birthday());
                employee.setFechaNacimiento(faker.date().birthday());
                session.save(employee);
            }
            
            // Commit de la transacci�n
            session.getTransaction().commit();

            // Registro de informaci�n sobre el n�mero total de registros
            Long totalRegistros = (Long) session.createQuery("SELECT COUNT(*) FROM Employee").uniqueResult();
            System.out.println("El n�mero total de registros en la tabla Employee es: " + totalRegistros);

            // Obtenci�n de todos los empleados
            List<Employee> empleats = session.createQuery("FROM Employee", Employee.class).getResultList();

            // Procesamiento de cada empleado
            for (Employee employee : empleats) {
                // Obtenci�n de la edad del empleado
                LocalDate fechaNacimiento = employee.getFechaNacimiento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                LocalDate ahora = LocalDate.now();
                long edad = ChronoUnit.YEARS.between(fechaNacimiento, ahora);

                // Modificaci�n del nombre seg�n la edad del empleado
                if (edad >= 16 && edad <= 18) {
                    employee.setNombre(employee.getNombre() + " (junior)");
                } else if (edad > 50 && edad < 66) {
                    employee.setNombre(employee.getNombre() + " (senior)");
                } else if (edad > 65) {
                    session.delete(employee);
                }
            }

            // Inicia una nueva transacci�n para las modificaciones
            session.getTransaction().begin();
            session.getTransaction().commit();

        } catch (Exception e) {
            logger.error("Error al procesar empleados: " + e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
            if (factory != null) {
                factory.close();
            }
        }
    }
}